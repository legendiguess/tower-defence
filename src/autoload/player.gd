extends Node

var inventory = {
	"tower": 1,
	"bear_trap": 15,
	"shield_with_spikes": 15,
	"spikes": 2
	}

func spend_item(item) -> bool:
	print(inventory)
	if inventory[item] > 0:
		inventory[item]-=1;
		SaveManager.save()
		return true;
	else:
		return false

var diamonds = 50

func enough_diamonds_check(how_much):
	if diamonds >= how_much:
		return true
	else:
		return false

func subtract_diamonds(value):
	if enough_diamonds_check(value):
		diamonds = diamonds - value
		return true
	else:
		return false

func add_diamods(value):
	diamonds += value