extends Node

var icons = {
		"tower": preload("res://assets/sprites/choose_tower.png"),
		"bear_trap": preload("res://assets/sprites/choose_traps_trap.png"),
		"shield_with_spikes": preload("res://assets/sprites/choose_traps_shield.png"),
		"spikes": preload("res://assets/sprites/choose_traps_spikes.png"),
	}
var texts = {
		"tower": "Main defense unit",
		"bear_trap": "Deals 10 dmg and slows down",
		"shield_with_spikes": "Turning damage back",
		"spikes": "Dealing 50 dmg per second",
	}
var names = {
		"tower": "Tower",
		"bear_trap": "Bear trap",
		"shield_with_spikes": "Shield",
		"spikes": "Spikes",
	}
var prices = {
		"tower": 30,
		"bear_trap": 10,
		"shield_with_spikes": 25,
		"spikes": 50,
	}