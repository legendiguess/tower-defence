extends Node

var thread
func _ready():
	thread = Thread.new()
	load_game()
	pass

func save():
	thread.start(self, "_save_game", "1")

func _save_game(_cch):
	var save_game = File.new()
	save_game.open("user://tdwip.save", File.WRITE)
	save_game.store_line(to_json(Player.inventory))
	save_game.close()
	
func load_game():
	var save_game = File.new()
	if not save_game.file_exists("user://tdwip.save"):
		 return # Error! We don't have a save to load.
	save_game.open("user://tdwip.save", File.READ)
	var current_line = parse_json(save_game.get_line())
	for i in current_line.keys():
		Player.inventory[i] =  current_line[i]
	save_game.close()