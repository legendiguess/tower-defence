extends Node

const sizes = {
		"tower": Vector2(3, 3),
		"bear_trap": Vector2(3, 3),
		"shield_with_spikes": Vector2(7, 7),
		"spikes": Vector2(7, 7),
	}