extends PanelContainer

var item_name

# Called when the node enters the scene tree for the first time.
func _ready():
	$HBoxContainer/Button.connect("pressed", self, "button_pressed")

func button_pressed():
	if Player.subtract_diamonds(Items.prices[item_name]) == true:
		Player.inventory[item_name] += 1