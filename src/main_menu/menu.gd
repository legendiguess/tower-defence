extends MarginContainer

enum State {Hidden, Inventory, Settings, Shop}
var _state = State.Hidden

onready var tween = get_node("/root/MainMenu/Tween")
onready var inventory_button = get_node("../BottomMenu/PanelContainer/MarginContainer/PanelContainer/MarginContainer/HBoxContainer/InventoryButton")
onready var settings_button = get_node("../BottomMenu/PanelContainer/MarginContainer/PanelContainer/MarginContainer/HBoxContainer/SettingsButton")
onready var shop_button = get_node("../BottomMenu/PanelContainer/MarginContainer/PanelContainer/MarginContainer/HBoxContainer/ShopButton")
onready var play_button = get_node("../BottomMenu/PanelContainer/MarginContainer/PanelContainer/MarginContainer/HBoxContainer/PlayButton")
# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("InventoryPanel").hide()
	get_node("SettingsPanel").hide()
	get_node("ShopPanel").hide()
	inventory_button.connect("button_up", self, "state_to_inventory")
	settings_button.connect("button_up", self, "state_to_settings")
	shop_button.connect("button_up", self, "state_to_shop")
	play_button.connect("button_up", self, "state_to_hidden")
	play_button.disabled = true

func state_to_hidden():
	play_button.disabled = true
	tween.remove(self, "rect_position:y")
	tween.interpolate_property(self, "rect_position:y", self.rect_position.y, get_viewport_rect().size.y, 0.25, Tween.TRANS_QUART, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	_set_state(State.Hidden)

func state_to_shop():
	shop_button.disabled = true
	$ShopPanel.on()
	_set_state(State.Shop)

func state_to_inventory():
	inventory_button.disabled = true
	$InventoryPanel.show()
	_set_state(State.Inventory)

func state_to_settings():
	settings_button.disabled = true
	$SettingsPanel.show()
	_set_state(State.Settings)

func _set_state(new_state):
	if _state == State.Hidden:
		play_button.disabled = false
		tween.remove(self, "rect_position:y")
		tween.interpolate_property(self, "rect_position:y", get_viewport_rect().size.y, 0, 0.25, Tween.TRANS_QUART, Tween.EASE_OUT)
		tween.start()
	elif _state == State.Inventory:
		inventory_button.disabled = false
		$InventoryPanel.hide()
	elif _state == State.Settings:
		settings_button.disabled = false
		$SettingsPanel.hide()
	elif _state == State.Shop:
		shop_button.disabled = false
		$ShopPanel.off()
	_state = new_state