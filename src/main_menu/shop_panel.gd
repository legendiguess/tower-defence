extends PanelContainer

var preload_item = preload("res://src/main_menu/shop_item/shop_item.tscn")

var first_time_opened = false
var thread 

func on():
	show()
	# Загружаем вещи в магазине только при первом открытии магазина
	if first_time_opened == false:
		first_time_opened = true
		thread = Thread.new()
		thread.start(self, "_load_items")

func _load_items(userdata):
	for item_name in Items.names.keys():
		var item = preload_item.instance()
		$VBoxContainer/MarginContainer2/PanelContainer/MarginContainer/ScrollContainer/VBoxContainer.call_deferred("add_child", item)
		item.get_node("HBoxContainer/VBoxContainer/Name").set_text(Items.names[item_name])
		item.get_node("HBoxContainer/VBoxContainer/Description").set_text(Items.texts[item_name])
		item.get_node("HBoxContainer/TextureRect").set_texture(Items.icons[item_name])
		item.get_node("HBoxContainer/Button").text = str(Items.prices[item_name])
		item.item_name = item_name
	return

func off():
	hide()