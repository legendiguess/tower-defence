extends Node
#
# Просьба писать логику сеансовой валюты только здесь
#
onready var upgrade_node = get_node("/root/Game/GUI/UpgradeMenu")
var selected_tower;
var money = 9999999
var tower_upgrade_cost = {
	
	"crossbow": [0,10,15,20],
	"cannon": [0,10,15,20],
	"spinner": [0,10,15,20],
	"foundation": [0,10,15,20]
}

func _ready():
	#get_node("/root/Game/GUI").change_money(money)
	pass
	
func transaction(upgrade, level_upgrade)-> bool:
	if (money - tower_upgrade_cost[upgrade][level_upgrade] > 0):
		money-=tower_upgrade_cost[upgrade][level_upgrade]
		update_gui_money(money)
		return true
	else:
		return false

func update_gui_money(new_money):
	get_node("../../GUI/MarginContainer/HBoxContainer/NinePatchRect/MarginContainer/HBoxContainer/Coin/Label").set_text(str(new_money))

func get_money(quantity):#функция для получения денег от смерти зомби
	money += quantity
	update_gui_money(money)

func set_money(quantity):
	money -= quantity
	update_gui_money(money)