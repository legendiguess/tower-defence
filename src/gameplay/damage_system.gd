extends Node

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func deal_damage(target, number,source):
	var new_damage = int(rand_range(number * 0.7, number))
	target._damage(new_damage, source)