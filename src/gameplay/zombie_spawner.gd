extends Node
#Powered by snodack

onready var scene = get_node("/root/Game/Enemys")
var zombies = [preload("res://src/gameplay/enemies/zombies/zombie/zombie.tscn"), 
preload("res://src/gameplay/enemies/zombies/zombie_strong/zombie_strong.tscn")] # Массив разных зомби
var area_spawn_pos = [Vector2(-5,-5), Vector2(-3,5)]

#Чьеми сыновьями становиться
func ready():
	randomize()

func spawn_zombie(nubmer):
	var zombie = zombies[nubmer].instance()
	scene.add_child(zombie)
	zombie.translation.x = rand_range(area_spawn_pos[0].x, area_spawn_pos[1].x)
	zombie.translation.z = rand_range(area_spawn_pos[0].y, area_spawn_pos[1].y)