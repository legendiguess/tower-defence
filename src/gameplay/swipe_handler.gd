extends Node

signal swiped(direction)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _input(event):
	if event is InputEventScreenDrag:
		if event.relative.x == 0:
			pass
		elif event.relative.x > 0:
			emit_signal("swiped", Vector2(1, 0))
		else:
			emit_signal("swiped", Vector2(-1, 0))

func on():
	set_process_input(true)

func off():
	set_process_input(false)