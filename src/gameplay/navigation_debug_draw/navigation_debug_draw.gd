extends ImmediateGeometry

var points = []
onready var navigation = get_node("../Navigation")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(_delta):
	# Отрисовать путь
	if Input.is_key_pressed(KEY_F1):
		clear()
		begin(1, null) #1 = is an enum for draw line, null is for text
		for i in range(points.size()):
			if i + 1 < points.size():
				var A = points[i]
				var B = points[i + 1]
				add_vertex(A)
				add_vertex(B)
		end()
	# Отрисовать точки
	if Input.is_key_pressed(KEY_F2):
		clear()
		begin(1, null)
		for i in navigation.astar.get_points().size():
			var point = navigation.astar.get_point_position(i)
			if navigation.astar.is_point_disabled(i):
				pass
			else:
				add_vertex(point + Vector3(0, 0.1, 0))
				add_vertex(point + Vector3(0, 0.2, 0))
		end()