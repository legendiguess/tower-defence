extends Spatial

var _speed
var _target_enemy_position

func new(tower, weapon_type, damage, enemy):
	$Area.queue_free()
	# Удаляем ненужные меши и ставими специфичные параметры
	if weapon_type == 1:
		$Arrow.queue_free()
		_speed = 10
	elif weapon_type ==2:
		$Cannonball.queue_free()
		_speed = 20
	#
	_target_enemy_position = enemy.translation
	self.translation = tower.translation + tower.get_node("Top").translation
	self.look_at(enemy.translation, Vector3(0, 1, 0))
	self.set_process(true)

func _process(delta):
	if !translation.distance_to(_target_enemy_position) < 0.05:
		self.translate(Vector3(0, 0, -delta * _speed))
	else:
		destroy()

func destroy():
	# Спавн партиклов различных
	queue_free()
