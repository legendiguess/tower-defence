extends Spatial

var _speed
var _damage
var _tower_position

func new(tower, weapon_type, damage):
	# Удаляем ненужные меши и ставими специфичные параметры
	if weapon_type == 1:
		$Arrow.queue_free()
		_speed = 10
	elif weapon_type ==2:
		$Cannonball.queue_free()
		_speed = 20
	#
	self.translation = tower.translation + tower.get_node("Top").translation
	_tower_position = self.translation
	self.rotation_degrees.y = get_node("/root/Game/Cameras/TowerCamera").rotation_degrees.y
	self.rotation_degrees.x = get_node("/root/Game/Cameras/TowerCamera").rotation_degrees.x
	_damage = damage
	$Area.connect("body_entered", self, "_body_entered")
	self.set_process(true)

func _process(delta):
	if self.translation.distance_to(_tower_position) > 20:
		destroy()
	self.translate(Vector3(0, 0, -delta * _speed))

func _body_entered(body):
	get_node("/root/Game/DamageSystem").deal_damage(body, _damage, self)
	destroy()

func destroy():
	# Спавн партиклов различных
	queue_free()