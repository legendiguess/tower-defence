extends Node
#Powered by snodack

#------------------------------
# Задается через init
var quantity = [[10, 4], [15, 5], [26, 6]]
var time_waves = [10, 10, 10] # Время волны
var wave_controller
var current_coming = 0

func _ready():
	wave_controller = get_node("WaveContoller")
	pass

func next_level():
	if current_coming < len(quantity):
		wave_controller.pushwave(quantity[current_coming], time_waves[current_coming])
		current_coming += 1
	else:
		get_node("/root/Game/LevelLoader").next_level()

func input_world_data(quant, time):
	quantity = quant
	time_waves = time

func _on_WaveContoller__end_wave_():
	pass