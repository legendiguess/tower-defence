extends Node

var astar = AStar.new()
export var CELL_SIZE = Vector2(0.25, 0.25) # Ширина и Высота
export var CELL_COUNT = Vector2(116, 36)#84
onready var CELL_START_POINT = get_node("/root/Game/FieldStartPoint").translation
onready var points_count

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("/root/Game/Camera").border_x = Vector2(CELL_START_POINT.x, CELL_START_POINT.x + CELL_SIZE.x * CELL_COUNT.x)
	for y in range(CELL_COUNT.y):
		for x in range(CELL_COUNT.x):
			#var point_2d = Vector2(CELL_START_POINT.x, CELL_START_POINT.y) + Vector2(x * CELL_SIZE.x, y * CELL_SIZE.y)
			var point = CELL_START_POINT + Vector3(x * CELL_SIZE.x, 0, y * CELL_SIZE.y)
			#astar.add_point(point_index(Vector2(x, y)), Vector3(point_2d.x, 0.5, point_2d.y))
			astar.add_point(point_index(Vector2(x, y)), point)
	for y in range(CELL_COUNT.y):
		for x in range(CELL_COUNT.x):
			var point = Vector2(x, y)
			if y != 0:
				astar.connect_points(point_index(point), point_index(point + Vector2(0, -1)), true)
			if y != CELL_COUNT.y-1:
				astar.connect_points(point_index(point), point_index(point + Vector2(0, 1)), true)
			if x != CELL_COUNT.x-1:
				astar.connect_points(point_index(point), point_index(point + Vector2(1, 0)), true)
			if x != 0:
				if y != 0:
					astar.connect_points(point_index(point), point_index(point + Vector2(-1, -1)), true)
			if x != CELL_COUNT.x-1:
				if y != CELL_COUNT.y-1:
					astar.connect_points(point_index(point), point_index(point + Vector2(1, 1)), true)
	points_count = astar.get_points().size()

func point_index(point):
	return point.x + CELL_COUNT.x * point.y

func get_path_for_enemy(point):
	var near = astar.get_closest_point(point)
	var target
	if point.x < 4:
		target = astar.get_closest_point(Vector3(4,0,point.z))
	else:
		target = astar.get_closest_point(Vector3(6,0,0))
	var way = astar.get_point_path(near, target)
	if len(way) == 0:
		way = [Vector3(10,0,0)]
	return way

func get_random_point_id():
	return int(rand_range(0, points_count))

func return_points_if_not_collide(point_translation, size_in_points):
	var points = []
	var half_cell_size = Vector2(int(size_in_points.x/2), int(size_in_points.y/2))
	var lu_point = point_translation - Vector3(CELL_SIZE.x * half_cell_size.x, 0, CELL_SIZE.y * half_cell_size.y)
	for y in size_in_points.y:
		for x in size_in_points.x:
			var point = lu_point + Vector3(CELL_SIZE.x * x, 0, CELL_SIZE.y * y)
			var closet_point = astar.get_closest_point(point)
			if !astar.is_point_disabled(closet_point):
				points.append(closet_point)
			else:
				return []
	return points

func block_rect_points(points):
	for point in points:
		astar.set_point_disabled(point, true)

func unblock_rect_points(point_translation, size_in_points):
	var half_cell_size = Vector2(int(size_in_points.x/2), int(size_in_points.y/2))
	var lu_point = point_translation - Vector3(CELL_SIZE.x * half_cell_size.x, 0, CELL_SIZE.y * half_cell_size.y)
	# Блокировка точек, которые закрывает новопостроенная башня
	for y in size_in_points.y:
		for x in size_in_points.x:
			var point = lu_point + Vector3(CELL_SIZE.x * x, 0, CELL_SIZE.y * y)
			astar.set_point_disabled(astar.get_closest_point(point), false)