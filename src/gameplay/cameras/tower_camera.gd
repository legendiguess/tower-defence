extends Camera

onready var main_cmr = get_node("../../Camera")
onready var color = get_node("/root/Game/ColorRect")
enum State {NotActive, GoingToTower, Attached, GoingToNotActive}
var state = State.NotActive
const FOLLOW_SPEED = 4.0
var tower = null
var ray_lenght = 1000

func _ready():
	set_process_input(false)
	set_process(false)
	pass

func _process(delta):
	if state == State.Attached:
		color.color.a = lerp(color.color.a, 0, FOLLOW_SPEED*delta)
		if is_instance_valid(tower):
			rotation_degrees.y = tower.get_node("Top").rotation_degrees.y +90#+ Vector3(0,90,0)
		else:
			state_going_to_not_active()
	elif state == State.GoingToTower:
		if !is_instance_valid(tower):
			state_going_to_not_active()
		color.color.a = lerp(color.color.a, 1, FOLLOW_SPEED*delta)
		var target_trans = tower.translation + Vector3(0, tower.get_node("Top").translation.y + 0.4, 0)
		translation = translation.linear_interpolate(target_trans, FOLLOW_SPEED*2*delta)
		#var th = tower.direction
		rotation_degrees = rotation_degrees.linear_interpolate(tower.get_node("Top").rotation_degrees + Vector3(0,90,0), FOLLOW_SPEED*delta)
		if (target_trans - translation).length() < 0.001:
			state_attached()
	elif state == State.GoingToNotActive:
		translation = translation.linear_interpolate(main_cmr.translation, FOLLOW_SPEED*delta)
		rotation_degrees = rotation_degrees.linear_interpolate(main_cmr.rotation_degrees, FOLLOW_SPEED*2*delta)
		if (main_cmr.translation - translation).length() < 0.1:
			state_not_active()
			pass
		pass
	pass

func set_begin_pos():
	translation =  main_cmr.translation
	rotation_degrees = main_cmr.rotation_degrees

func target_tower(_tower):
	set_begin_pos()
	tower = _tower
	state_going_to_tower()
	pass

func state_not_active():
	current = false
	main_cmr.state_active()
	set_state(State.NotActive)
	set_process(false)
	

func state_going_to_tower():
	get_node("/root/Game/GUI/TowerExitButton").show()
	set_process(true)
	main_cmr.state_not_active()
	current = true
	set_state(State.GoingToTower)
	pass

func state_attached():
	translation = tower.translation + Vector3(0, tower.get_node("Top").translation.y + 0.4, 0)
	rotation_degrees = tower.get_node("Top").rotation_degrees + Vector3(0,90,0)
	tower.state_hand_control()
	set_process_input(true)
	set_state(State.Attached)
	pass

func state_going_to_not_active():
	rotation_degrees.x = 0
	get_node("/root/Game/GUI/TowerExitButton").hide()
	if is_instance_valid(tower):
		tower.state_auto()
	set_state(State.GoingToNotActive)
	set_process_input(false)
	pass

func set_state(value):
	state = value

func _input(event):
	if event is InputEventScreenDrag:
		if is_instance_valid(tower):
			tower.hand_rotate(event.relative)
			updown(event.relative)
		return
func updown(relative):
	rotation_degrees.x -=relative.y/10
	if rotation_degrees.x < -30:
		rotation_degrees.x = -30
	elif rotation_degrees.x > 30:
		rotation_degrees.x = 30