extends InterpolatedCamera

const sensy =0.3 # Чувствительность y
const sensx =0.075
var alpha = 90
var hmax = 8 # Максимальная высота камеры /максимальная отдаленность от центра
var border_x = Vector2(-5, 5)
enum State {not_active, active}
var state = State.keys()[State.active]
var delta_h = 2 # Минимальная высота камеры
const RAY_LENGHT = 1000

#smooth
var trans_x
func _ready():
	trans_x = translation.x
	new_transform()
	pass # Replace with function body.

func new_transform(): # Да, я это сам придумал
	if alpha > 90: alpha = 90
	if alpha < 0: alpha = 0
	rotation_degrees.x = -alpha
	translation.y = hmax * sin(deg2rad(alpha)) + delta_h
	translation.z = hmax * cos(deg2rad(alpha))
	pass

func move_x(delta):
	var cch = translation.x + delta
	if cch < border_x.x: cch = border_x.x
	if cch > border_x.y: cch = border_x.y
	trans_x = cch
	
func _process(_delta):
	translation.x = lerp(translation.x, trans_x, 8*_delta)
	pass

func _input(event):
	if event is InputEventScreenDrag and event.relative.length()>0.5 and get_node("/root/Game/GUI/").state == get_node("/root/Game/GUI/").State.None:
		if abs(event.relative.x) > abs(event.relative.y):
			move_x(-event.relative.x * sensx)
		else:
			alpha+= (event.relative.y) * sensy # Хорошо бы тут брать разрешения экрана
			new_transform()

func state_not_active():
	current = false
	setState(State.keys()[State.not_active])
	set_process(false)
	set_process_input(false)

func state_active():
	current = true
	setState(State.keys()[State.active])
	set_process(true)
	set_process_input(true)

func setState(value):
	state = value
func prir():
	pass

# Функция возвращает результат рейкаста от камеры
# layer - "слой" на который нужно рейкастить. 
# на 1 слое находятся: игровая плоскость
# на 2 слое находятся: башни
# на 3 слое находятся: враги
func ray_cast(layer):
	var mouse_position = get_viewport().get_mouse_position()
	var from = self.project_ray_origin(mouse_position)
	var to = from + self.project_ray_normal(mouse_position) * RAY_LENGHT
	var space_state = self.get_world().get_direct_space_state()
	return space_state.intersect_ray(from, to, [], layer)