extends Popup

var selected_tower;
var crossbow_text = ("Rate of fire: ");
var cannon_text = ("Damage: ");
var spinner_text = ("Aim speed: ");
var foundation_text = ("Health: ");
var plus_string = " → ";

onready var tower_upgrades = get_node("/root/Game/BuildingSystem").tower_upgrades
onready var crossbow_stat = get_node("MarginContainer/Upgrademods/VBoxContainer/MarginContainer/PanelContainer/HBoxContainer/HBoxContainer2/MainWeaponPanel/HBoxContainer/VBoxContainer2/Stat")
onready var cannon_stat = get_node("MarginContainer/Upgrademods/VBoxContainer/MarginContainer/PanelContainer/HBoxContainer/HBoxContainer2/MainWeaponPanel/HBoxContainer/VBoxContainer/Stat")
onready var spinner_stat = get_node("MarginContainer/Upgrademods/VBoxContainer/MarginContainer/PanelContainer/HBoxContainer/HBoxContainer/VBoxContainer/Panel2/HBoxContainer/VBoxContainer/Stat")
onready var foundation_stat = get_node("MarginContainer/Upgrademods/VBoxContainer/MarginContainer/PanelContainer/HBoxContainer/HBoxContainer/VBoxContainer/Panel1/HBoxContainer/VBoxContainer/Stat")

onready var coins_controller = get_node("/root/Game/Controllers/Coin_controller")

onready var crossbow_button = get_node("MarginContainer/Upgrademods/VBoxContainer/MarginContainer/PanelContainer/HBoxContainer/HBoxContainer2/MainWeaponPanel/HBoxContainer/VBoxContainer2/CrossbowButton")
onready var cannon_button = get_node("MarginContainer/Upgrademods/VBoxContainer/MarginContainer/PanelContainer/HBoxContainer/HBoxContainer2/MainWeaponPanel/HBoxContainer/VBoxContainer/CannonButton")
onready var spinner_button = get_node("MarginContainer/Upgrademods/VBoxContainer/MarginContainer/PanelContainer/HBoxContainer/HBoxContainer/VBoxContainer/Panel2/HBoxContainer/VBoxContainer/SpinnerButton")
onready var foundation_button = get_node("MarginContainer/Upgrademods/VBoxContainer/MarginContainer/PanelContainer/HBoxContainer/HBoxContainer/VBoxContainer/Panel1/HBoxContainer/VBoxContainer/FoundationButton2")

onready var crossbow_box = get_node("MarginContainer/Upgrademods/VBoxContainer/MarginContainer/PanelContainer/HBoxContainer/HBoxContainer2/MainWeaponPanel/HBoxContainer/VBoxContainer2")
onready var cannon_box = get_node("MarginContainer/Upgrademods/VBoxContainer/MarginContainer/PanelContainer/HBoxContainer/HBoxContainer2/MainWeaponPanel/HBoxContainer/VBoxContainer")
onready var label_or = get_node("MarginContainer/Upgrademods/VBoxContainer/MarginContainer/PanelContainer/HBoxContainer/HBoxContainer2/MainWeaponPanel/HBoxContainer/Label")

onready var building_system = get_node("/root/Game/BuildingSystem")

func _ready():
	crossbow_button.connect("button_up", self, "_on_CrossbowButton_button_up")
	cannon_button.connect("button_up", self, "_on_CannonButton_button_up")
	spinner_button.connect("button_up", self, "_on_SpinnerButton_button_up")
	foundation_button.connect("button_up", self, "_on_FoundationButton_button_up")
	get_node("MarginContainer/Upgrademods/VBoxContainer/HBoxContainer/CrossButton").connect("button_up", self, "close")
	get_node("MarginContainer/Upgrademods/VBoxContainer/MarginContainer/PanelContainer/HBoxContainer/HBoxContainer2/SellTowerButton").connect("button_up", self, "_on_SellTowerButton_button_up")

func stat_insert():
	var result = stat_insert_check("crossbow")
	if str(result) != "max":
		result = 60/result
	crossbow_stat.set_text(crossbow_text + str(selected_tower.get_info("crossbow")) + plus_string + str(result))
	cannon_stat.set_text(cannon_text + str(selected_tower.get_info("cannon")) + plus_string + str(stat_insert_check("cannon")))
	spinner_stat.set_text(spinner_text + str(selected_tower.get_info("spinner")) + plus_string + str(stat_insert_check("spinner")))
	foundation_stat.set_text(foundation_text + str(selected_tower.get_info("foundation")) + plus_string + str(stat_insert_check("foundation")))
	
func stat_insert_check(module_name):
	if module_name == "crossbow" and selected_tower._level_upgrade[module_name] >= 1:
		cannon_box.set_visible(false)
		label_or.set_visible(false)
	if module_name == "cannon" and selected_tower._level_upgrade[module_name] >= 1:
		crossbow_box.set_visible(false)
		label_or.set_visible(false)
	if selected_tower._level_upgrade[module_name] == 0:
		return tower_upgrades[module_name][0]
	if selected_tower._level_upgrade[module_name] == 1:
		return tower_upgrades[module_name][1]
	if selected_tower._level_upgrade[module_name] == 2:
		return tower_upgrades[module_name][2]
	if selected_tower._level_upgrade[module_name] == 3:
		return tower_upgrades[module_name][3]
	if selected_tower._level_upgrade["crossbow"] == building_system.tower_upgrades["crossbow"].size():
		crossbow_button.disabled = true
	if selected_tower._level_upgrade["cannon"] == building_system.tower_upgrades["cannon"].size():
		cannon_button.disabled = true
	if selected_tower._level_upgrade["spinner"] == building_system.tower_upgrades["spinner"].size():
		spinner_button.disabled = true
	if selected_tower._level_upgrade["foundation"] == building_system.tower_upgrades["foundation"].size():
		foundation_button.disabled = true
	return "max"
func unblock_buttons():
	crossbow_button.disabled = false
	cannon_button.disabled = false
	spinner_button.disabled = false
	foundation_button.disabled = false
	cannon_box.set_visible(true)
	crossbow_box.set_visible(true)
	label_or.set_visible(true)
	
func _process(_delta):
	if !is_instance_valid(selected_tower):
		close()
		
func close():
	selected_tower = null
	hide()

func _on_FoundationButton_button_up():
	if is_instance_valid(selected_tower):
		selected_tower.upgrade("foundation")
	close()

func _on_SpinnerButton_button_up():
	if is_instance_valid(selected_tower):
		selected_tower.upgrade("spinner")
	close()

func _on_CannonButton_button_up():
	if is_instance_valid(selected_tower):
		selected_tower.upgrade("cannon")
	close()

func _on_CrossbowButton_button_up():
	if is_instance_valid(selected_tower):
		selected_tower.upgrade("crossbow")
	close()

func _on_CameraButton_button_up():
	if is_instance_valid(selected_tower):
		get_node("../../Cameras/TowerCamera").target_tower(selected_tower)
	close()
	
func _on_SellTowerButton_button_up():
	if is_instance_valid(selected_tower):
		var summ = 0
		summ += building_system.tower_cost
		print(summ)
		summ += coins_controller.tower_upgrade_cost["crossbow"][selected_tower._level_upgrade["crossbow"]]
		print(coins_controller.tower_upgrade_cost["crossbow"][selected_tower._level_upgrade["crossbow"]])
		summ += coins_controller.tower_upgrade_cost["cannon"][selected_tower._level_upgrade["cannon"]]
		print(coins_controller.tower_upgrade_cost["cannon"][selected_tower._level_upgrade["cannon"]])
		summ += coins_controller.tower_upgrade_cost["spinner"][selected_tower._level_upgrade["spinner"]]
		print(coins_controller.tower_upgrade_cost["spinner"][selected_tower._level_upgrade["spinner"]])
		summ += coins_controller.tower_upgrade_cost["foundation"][selected_tower._level_upgrade["foundation"]]
		print(coins_controller.tower_upgrade_cost["foundation"][selected_tower._level_upgrade["foundation"]])
		
		coins_controller.get_money(summ/2)
		selected_tower.destroy()