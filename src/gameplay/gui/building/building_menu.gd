extends HBoxContainer

enum State {Minimized, WithDescription}
var state
var ray_lenght = 1000

onready var game = get_node("/root/Game")
onready var apply_button = $Buttons/ApplyButton
onready var info_button = $Inventory/HBoxContainer/InfoButton
onready var return_button = $Buttons/ReturnButton
onready var building_button = $Buttons/BuildingButton

var preload_item = preload("res://src/gameplay/gui/building/inventory/item.tscn")

const building_preview = {
	"tower": preload("res://src/gameplay/buildings/previews/tower.tscn"),
	"bear_trap": preload("res://src/gameplay/buildings/previews/bear_trap.tscn"),
	# ! Нужно заменить
	"shield_with_spikes": preload("res://src/gameplay/buildings/previews/spikes.tscn"),
	#
	"spikes": preload("res://src/gameplay/buildings/previews/spikes.tscn"),
}

var preview = null

func state_to_with_description():
	items_description(true)
	info_button.text = ">"
	_change_state(State.WithDescription)

func state_to_minimized():
	items_description(false)
	info_button.text = "<"
	_change_state(State.Minimized)

func _change_state(new_state):
	state = new_state
	set_anchors_and_margins_preset(Control.PRESET_RIGHT_WIDE, Control.PRESET_MODE_MINSIZE)

func load_items():
	var i = 0
	for index in Player.inventory.keys():
		if Player.inventory[index] > 0:
			var instaced_item = preload_item.instance()
			if Player.inventory[index] >= 1:
				$Inventory/HBoxContainer/HBoxContainer/MarginContainer/PanelContainer/ScrollContainer/Items.add_child(instaced_item)
				instaced_item.get_node("HBoxContainer/Description").set_text(Items.texts[index])
				instaced_item.get_node("HBoxContainer/ItemButton/TextureRect").set_texture(Items.icons[index])
				instaced_item.id = index

func _ready():
	print(Player.inventory.keys())
	load_items()
	info_button.connect("pressed", self, "info_button_pressed")
	apply_button.connect("pressed", self, "apply_button_pressed")
	state_to_minimized()

func _process(delta):
	for item in $Inventory/HBoxContainer/HBoxContainer/MarginContainer/PanelContainer/ScrollContainer/Items.get_children():
		if item.get_node("HBoxContainer/ItemButton").is_pressed():
			item_pressed(item.id)
	if preview != null:
		if Input.is_mouse_button_pressed(1):
			var result = game.camera.ray_cast(1)
			if !result.empty():
				preview.move(result["position"])
				if preview.type == true:
					apply_button.show()
				else:
					apply_button.hide()

func apply_button_pressed():
	if preview.item_id == "tower":
		game.building_system.build_tower(preview.translation)
	else:
		game.building_system.buld_trap(preview.item_id, preview.translation)

func item_pressed(item_id):
	if self.preview != null:
		self.preview.queue_free()
	preview = building_preview[item_id].instance()
	preview.item_id = item_id
	get_node("/root/Game/Towers").add_child(preview)
	preview.size = Buildings.sizes[item_id]
	# 
	var center_of_screen = get_viewport_rect().size
	center_of_screen = Vector2(center_of_screen.x / 2, center_of_screen.y / 2)
	var from = game.camera.project_ray_origin(center_of_screen)
	var to = from + game.camera.project_ray_normal(center_of_screen) * ray_lenght
	var space_state = game.camera.get_world().get_direct_space_state()
	var result = space_state.intersect_ray(from, to, [], 1)
	#  ! ! !
#	if !result.empty():
#		preview.move(result["position"])

func info_button_pressed():
	if state == State.Minimized:
		state_to_with_description()
	else:
		state_to_minimized()

func items_description(boolean):
	for child in get_node("Inventory/HBoxContainer/HBoxContainer/MarginContainer/PanelContainer/ScrollContainer/Items").get_children():
			child.get_node("HBoxContainer/Description").visible = boolean