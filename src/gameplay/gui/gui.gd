extends Control

enum State {None, Building, Upgrade}

onready var touch_handler = get_node("../TouchHandler")
var state = State.Building

func state_to_upgrade():
	$UpgradeMenu.show()
	_change_state(State.Upgrade)

func state_to_building():
	$BuildingMenu/Inventory.show()
	$BuildingMenu/Buttons/ApplyButton
	$BuildingMenu.return_button.show()
	$BuildingMenu/Buttons/BuildingButton.hide()
	$BuildingMenu.items_description(false)
	$BuildingMenu.set_anchors_and_margins_preset(Control.PRESET_RIGHT_WIDE, Control.PRESET_MODE_MINSIZE)
	_change_state(State.Building)

func state_to_none():
	_change_state(State.None)

func _change_state(new_state):
	if state == State.None:
		touch_handler.lock()
	else:
		touch_handler.unlock()
	if state == State.Building:
		$BuildingMenu.state_to_minimized()
		$BuildingMenu/Inventory.hide()
		$BuildingMenu/Buttons/ApplyButton.hide()
		$BuildingMenu.return_button.hide()
		$BuildingMenu/Buttons/BuildingButton.show()
		$BuildingMenu.set_anchors_and_margins_preset(Control.PRESET_RIGHT_WIDE, Control.PRESET_MODE_MINSIZE)
		if $BuildingMenu.preview != null:
			$BuildingMenu.preview.queue_free()
			$BuildingMenu.preview = null
	elif state == State.Upgrade:
		$Upgrade.hide()
	state = new_state

# Called when the node enters the scene tree for the first time.
func _ready():
	$BuildingMenu.building_button.connect("pressed", self, "state_to_building")
	$BuildingMenu.return_button.connect("pressed", self, "state_to_none")
	state_to_none()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

#extends Control
#
#func _ready():
#	#$TowerExitButton.hide()
#
#func change_money(new_money):
#	get_node("MarginContainer/HBoxContainer/NinePatchRect/MarginContainer/HBoxContainer/Coin/Label").set_text(str(new_money))
#
func _on_TowerExitButton_button_up():
	get_node("/root/Game/Cameras/TowerCamera").state_going_to_not_active()
	pass # Replace with function body.
	#return  (get_node("BuildMenu").visible or get_node("UpgradeMenu").visible or get_node("Building").visible)