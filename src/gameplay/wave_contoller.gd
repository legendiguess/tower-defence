extends Node
#Powered by snodack
signal _end_wave_
var zombie_quantity = [0,0]
var wave_time = 0
var wave_coming = false

var zombie_spawner
# ----------------------------
# Дополнительный переменные
var teak =  0.25 #Тики появления зомби
var diff = 4.0 # чем больше число, тем больше выйдет в конце
var cch_teak = teak
var cch_time = 0
# ----------------------------
onready var rng = RandomNumberGenerator.new()
func _ready():
	zombie_spawner = get_node("../ZombieSpawner")
	rng.randomize()
	pass


func _process(delta):
	if !wave_coming:
		return
	cch_teak -=delta
	if cch_teak < 0:
		spawn_zombie()
		cch_teak = teak

func pushwave(z_q, w_t):
	zombie_quantity = z_q
	wave_time = w_t
	wave_coming = true
	cch_time = 0

func spawn_zombie():
	cch_time+=teak
	if wave_time > 1:
		for i in range(len(zombie_quantity)):
			if zombie_quantity[i]!=0 and pow(0.0 + cch_time/wave_time, diff) > rng.randf_range(0.0, 1.0):
				zombie_quantity[i]-=1
				zombie_spawner.spawn_zombie(i)
	else:
		wave_coming = false
		#Вызов сигнала что волна кончилась
		for i in range(len(zombie_quantity)):
			for _j in range(0, zombie_quantity[i]):
				zombie_spawner.spawn_zombie(i)
		emit_signal("_end_wave_")