extends Spatial

var type = false
onready var game = get_node("/root/Game")
var size = Vector2()
export var item_id = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func ok():
	type = true
	$Bad.hide()
	$Ok.show()

func bad():
	type = false
	$Ok.hide()
	$Bad.show()

func move(new_translation):
	self.translation = game.navigation.astar.get_point_position(game.navigation.astar.get_closest_point(new_translation))
	var result = game.navigation.return_points_if_not_collide(self.translation, Buildings.sizes[item_id])
	if result != []:
		ok()
		return
	bad()