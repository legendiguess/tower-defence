extends "res://src/gameplay/buildings/traps/trap.gd"

const DAMAGE = 5

#---------------------
var radius

func _ready():
	trap_name = "bear_trap"
	radius = 0.375

func _process(_delta):
	check_targets()

func check_targets():
	var pos = Vector2(translation.x, translation.z)
	for enemy in get_tree().get_nodes_in_group("enemys"):
		var enemy_position = Vector2(enemy.translation.x, enemy.translation.z)
		if pos.distance_to(enemy_position) - enemy.ZOMBIE_RADIUS < radius:
			get_node("/root/Game/DamageSystem").deal_damage(enemy, DAMAGE, self)
			enemy.speed /=2
			print(" ЧИК ЧИК")
			set_process(false)
			#проиграть анимация
			self.destroy()