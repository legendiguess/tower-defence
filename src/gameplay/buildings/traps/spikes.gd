extends "res://src/gameplay/buildings/traps/trap.gd"

const DAMAGE = 5
const DEFAULT_TEAK = 0.5
#---------------------
var hp = 10# Кол-во атак по зомби
var radius
var teak = DEFAULT_TEAK

func _ready():
	trap_name = "spikes"
	radius = 0.875


func _process(delta):
	teak -= delta
	if teak < 0:
		check_targets()
		teak =  DEFAULT_TEAK

func check_targets():
	var pos = Vector2(translation.x, translation.z)
	var dmg_flag = false
	for enemy in get_tree().get_nodes_in_group("enemys"):
		var enemy_position = Vector2(enemy.translation.x, enemy.translation.z)
		if pos.distance_to(enemy_position) - enemy.ZOMBIE_RADIUS < radius:
			get_node("/root/Game/DamageSystem").deal_damage(enemy, DAMAGE,self)
			dmg_flag = true
	if dmg_flag:
		hp-=1
		if (hp< 0):
			#Проиграть анимацию
			self.destroy()