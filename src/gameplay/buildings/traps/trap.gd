extends Spatial

var trap_name = ""
onready var navigation = get_node("/root/Game/Navigation")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func destroy():
	navigation.unblock_rect_points(translation, Buildings.sizes[trap_name])
	self.queue_free()