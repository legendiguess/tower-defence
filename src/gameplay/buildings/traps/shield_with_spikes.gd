extends "res://src/gameplay/buildings/traps/trap.gd"

var _max_hp = 100
var _hp = _max_hp

func _ready():
	trap_name = "shield_with_spikes"

func _damage(amount, source):
	_hp -= amount
	get_node("/root/Game/DamageSystem").deal_damage(source, amount,self)
	if _hp <= 0:
		self.destroy()