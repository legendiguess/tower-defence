extends StaticBody

signal tower_broken
signal tower_create
enum State {Auto, HandControl}
enum MainWeapon {None, Cannon, Crossbow}
var _max_hp = 100
var _hp = _max_hp
var _level_upgrade = {
	"crossbow": 0,
	"cannon": 0,
	"spinner": 0,
	"foundation": 0
}
var _damage = 0
var _attack_range = Vector2()
var direction = Vector2(-1, 0)
var state = State.Auto
var target_rot  = null
var _main_weapon = MainWeapon.None
var _rotating_speed = 0
var _shoot_timer_max = 50
var _shoot_timer = 0

onready var game = get_node("/root/Game")
var target = null

onready var coin_controller = get_node("/root/Game/Controllers/Coin_controller")
onready var navigation = get_node("/root/Game/Navigation")
onready var building = get_node("/root/Game/BuildingSystem")

func state_auto():
	if _main_weapon != MainWeapon.None:
		get_node("/root/Game/GUI/TowerSight").hide()
		if get_node("Top").get_child_count() > 0:
			for child in get_node("Top").get_children():
				if "MainWeapon" in child.name:
					child.show()
	get_node("/root/Game/TouchHandler").unlock()
	_set_state(State.Auto)

func state_hand_control():
	if _main_weapon != MainWeapon.None:
		get_node("/root/Game/GUI/TowerSight").show()
		if get_node("Top").get_child_count() > 0:
			for child in get_node("Top").get_children():
				if "MainWeapon" in child.name:
					child.hide()
	target_rot = get_node("Top").rotation_degrees
	get_node("/root/Game/TouchHandler").lock()
	_set_state(State.HandControl)

func _set_state(new_state):
	# Делаем вещички перед переходом на новое состояние:
	if self.state == State.Auto:
		pass
	elif self.state == State.HandControl:
		pass
	self.state = new_state

func _set_main_weapon(weapon):
	if _main_weapon == MainWeapon.None:
		if weapon == MainWeapon.Cannon:
			_damage = 100;
			_shoot_timer_max = 50
			_shoot_timer = _shoot_timer_max
			_attack_range = Vector2(1.75, 3)
		elif weapon == MainWeapon.Crossbow:
			_damage = 10;
			_shoot_timer_max = 10
			_shoot_timer = _shoot_timer_max
			_attack_range = Vector2(0, 4)
		_main_weapon = weapon

func upgrade(string):
	if !coin_controller.transaction(string, _level_upgrade[string]-1):# не хватает деняг
		return false
	_level_upgrade[string] += 1
	var new_module = building.tower_modules[string][_level_upgrade[string]-1].instance()
	if string == "crossbow":
		if get_node("Top").get_child_count() > 0:
			for child in get_node("Top").get_children():
				if "MainWeapon" in child.name:
					child.queue_free()
		_set_main_weapon(MainWeapon.Crossbow)
		$Top.call_deferred("add_child", new_module)
		_shoot_timer_max = building.tower_upgrades[string][_level_upgrade[string]-1]
		_shoot_timer = _shoot_timer_max
	elif string == "cannon":
		if get_node("Top").get_child_count() > 0:
			for child in get_node("Top").get_children():
				if "MainWeapon" in child.name:
					child.queue_free()
		_set_main_weapon(MainWeapon.Cannon)
		$Top.call_deferred("add_child", new_module)
		_damage = building.tower_upgrades[string][_level_upgrade[string]-1]
	elif string == "spinner":
		if get_node("Top").get_child_count() > 0:
			for child in get_node("Top").get_children():
				if string in child.name.to_lower():
					child.queue_free()
		$Top.call_deferred("add_child", new_module)
		self._rotating_speed = building.tower_upgrades[string][_level_upgrade[string]-1]
	elif string == "foundation":
		if get_child_count() > 3:
			for child in self.get_children():
				if "Foundation" in child.name:
					child.queue_free()
		self.add_child(new_module)
		_max_hp = building.tower_upgrades[string][_level_upgrade[string]-1]
		_hp = _max_hp

func _ready():
	self.connect("tower_create", get_node("/root/Game/Enemys/"), "_on_Tower_tower_create")
	self.connect("tower_broken", get_node("/root/Game/Enemys/"), "_on_Tower_tower_broken")
	emit_signal("tower_create")

func _physics_process(_delta):
	if _shoot_timer > 0:
			_shoot_timer -= 1
	if state == State.HandControl:
		get_node("Top").rotation_degrees.y = lerp(get_node("Top").rotation_degrees.y, target_rot.x, 8 * _delta)
		get_node("Top").rotation_degrees.z = lerp(get_node("Top").rotation_degrees.z, target_rot.z, 8 * _delta)
		if _shoot_timer == 0:
			if get_node("/root/Game/GUI/TowerSight").pressed:
				shoot(null)
				_shoot_timer = _shoot_timer_max
		return
	elif _main_weapon != MainWeapon.None:
		var position = Vector2(translation.x, translation.z)
		if is_instance_valid(target):
			get_node("Top").rotation_degrees.y = rad2deg(atan2(direction.y, -direction.x))
			var enemy_position = Vector2(target.translation.x, target.translation.z)
			var enemy_distance_to_tower = enemy_position.distance_to(position)
			var ap = (enemy_position - position).normalized()
			if enemy_distance_to_tower < _attack_range.y:
				if _attack_range.x < enemy_distance_to_tower:
					if ap.dot(direction) > 0.95:
						if ap.dot(direction) > 0.99:
							direction = (enemy_position - position).normalized()
						if _shoot_timer == 0:
							shoot(target)
							_shoot_timer = _shoot_timer_max
					else:
						direction.x = lerp(direction.x, ap.x,  _rotating_speed * _delta)
						direction.y = lerp(direction.y, ap.y,  _rotating_speed * _delta)
					return
		var target_min = null
		var target_min_angle = null
		for enemy in get_tree().get_nodes_in_group("enemys"):
			var enemy_position = Vector2(enemy.translation.x, enemy.translation.z)
			var enemy_distance_to_tower = enemy_position.distance_to(position)
			var ap = (enemy_position - position).normalized()
			if enemy_distance_to_tower < _attack_range.y:
				if _attack_range.x < enemy_distance_to_tower:
					if ap.dot(direction) > 0.95:
						target = enemy
						break
					else:
						var k = ap.angle_to(direction)
						if !is_instance_valid(target_min):
							target_min = enemy
							target_min_angle = k
						if abs(k) < abs(target_min_angle):
							target_min = enemy
							target_min_angle = k
							pass
		target = target_min

func _rotate_main_weapon_to_enemy(enemy, _delta):
	if !is_instance_valid(enemy): return;
	var tower_vector = Vector2(self.translation.x, self.translation.z)
	var enemy_position = Vector2(enemy.translation.x, enemy.translation.z)
	var tower_to_enemy = (enemy_position -tower_vector).normalized()
	direction = direction.linear_interpolate(tower_to_enemy, _rotating_speed*_delta* 50).normalized()

func _damage(amount, _source):
	_hp -= amount
	if _hp <= 0:
		destroy()

func destroy():
	get_node("/root/Game/GUI/TowerSight").hide()
	navigation.unblock_rect_points(translation, Buildings.sizes["tower"])
	emit_signal("tower_broken")
	self.queue_free()

# Передаем в enemy null если стреляем из ручного режима
func shoot(enemy):
	var new_ammo = game.preload_scenes["cannonball_and_arrow"].instance()
	game.add_child(new_ammo)
	if state == State.Auto:
		new_ammo.set_script(game.preload_scripts["auto_ammo"])
		new_ammo.new(self, _main_weapon, _damage, enemy)
		get_node("/root/Game/DamageSystem").deal_damage(enemy, _damage, self)
	elif state == State.HandControl:
		new_ammo.set_script(game.preload_scripts["hand_control_ammo"])
		new_ammo.new(self, _main_weapon, _damage)

func get_info(module_name):
	if module_name == "crossbow":
		return 60/(_shoot_timer_max)
	if module_name == "cannon":
		return _damage
	if module_name == "spinner":
		return _rotating_speed
	if module_name == "foundation":
		return _max_hp

func hand_rotate(relative):#angle):
	target_rot.x +=relative.x/10