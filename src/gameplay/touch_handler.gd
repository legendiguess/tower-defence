extends Node

var _locked = false

enum State {Nothing, JustPressed, Moved}

var _state = State.Nothing
var _params = [
	# Nothing
	{
			
	},
	# JustPressed
	{
		"timer": 0,
		"timer_finish": 10,
		"mouse_position": 0,
		"epsilon": 5
	},
	# Moved
	{
		
	}
]

onready var game = get_node("/root/Game")

func lock():
	_locked = true

func unlock():
	_locked = false

func state_to_nothing():
	_set_state(State.Nothing)

func state_to_just_pressed():
	_params[State.JustPressed].mouse_position = get_viewport().get_mouse_position()
	_set_state(State.JustPressed)

func state_to_moved():
	_set_state(State.Moved)

func _set_state(new_state):
	if _state == State.JustPressed:
		_params[_state].timer = 0
	_state = new_state

func _process(_delta):
	if _locked == true:
		return
	if _state == State.JustPressed:
		if Input.is_mouse_button_pressed(1):
			# Переход в Moved
			if get_viewport().get_mouse_position().distance_to(_params[_state].mouse_position) > _params[_state].epsilon:
				state_to_moved()
				return
			var result = game.camera.ray_cast(2)
			if !result.empty():
				var collider = result["collider"]
				game.upgrade_menu.selected_tower = collider
				game.upgrade_menu.unblock_buttons()
				game.upgrade_menu.stat_insert()
				game.camera.move_x(-(game.camera.translation.x - game.upgrade_menu.selected_tower.translation.x))
				game.upgrade_menu.show()
				state_to_nothing()
				return
			_params[_state].timer += 1
		else:
			state_to_nothing()
	elif _state == State.Nothing:
		if Input.is_mouse_button_pressed(1):
			state_to_just_pressed()
		return
	elif _state == State.Moved:
		if Input.is_mouse_button_pressed(1):
			return
		else:
			state_to_nothing()