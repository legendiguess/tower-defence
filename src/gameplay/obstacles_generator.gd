extends Node

export var obstacles_amount = 1
const AMOUNT_OF_TRIES = 5
var obstacles_paths = ["res://assets/scenes/obstacles/0.tscn", "res://assets/scenes/obstacles/1.tscn",
"res://assets/scenes/obstacles/2.tscn", "res://assets/scenes/obstacles/3.tscn", "res://assets/scenes/obstacles/4.tscn",
"res://assets/scenes/obstacles/5.tscn", "res://assets/scenes/obstacles/6.tscn", "res://assets/scenes/obstacles/7.tscn",
"res://assets/scenes/obstacles/8.tscn", "res://assets/scenes/obstacles/9.tscn", "res://assets/scenes/obstacles/10.tscn",
"res://assets/scenes/obstacles/11.tscn", "res://assets/scenes/obstacles/12.tscn", "res://assets/scenes/obstacles/13.tscn",
"res://assets/scenes/obstacles/14.tscn"]
onready var navigation = get_node("/root/Game/Navigation")
onready var obstacles = get_node("/root/Game/Obstacles")

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	generate_obstacles()

func generate_obstacles():
	for counter in obstacles_amount:
		var random_point = navigation.get_random_point_id()
		var count_of_tries = 0
		while navigation.astar.is_point_disabled(random_point):
			if count_of_tries > AMOUNT_OF_TRIES:
				return
			random_point = navigation.get_random_point_id()
			count_of_tries += 1
		var new_obstacle = load(obstacles_paths[randi()%int(obstacles_paths.size())]).instance() # obstacles_paths[randi()%int(obstacles_paths.size())]
		var half_points_size = Vector2(int(new_obstacle.obstacle_size_in_points.x/2), int(new_obstacle.obstacle_size_in_points.y/2))
		var lu_point = navigation.astar.get_point_position(random_point) - Vector3(navigation.CELL_SIZE.x * half_points_size.x, 0, navigation.CELL_SIZE.y * half_points_size.y)
		var points_to_block = []
		var need_to_restart = false
		for y in new_obstacle.obstacle_size_in_points.y:
			for x in new_obstacle.obstacle_size_in_points.x:
				var point_position = lu_point + Vector3(navigation.CELL_SIZE.x * x, 0, navigation.CELL_SIZE.y * y)
				var point = navigation.astar.get_closest_point(point_position)
				if navigation.astar.is_point_disabled(point) == false:
					points_to_block.append(point)
				else:
					need_to_restart = true
					break
		if need_to_restart:
			counter -= 1
		else:
			for point in points_to_block:
				navigation.astar.set_point_disabled(point, true)
			obstacles.add_child(new_obstacle)
			new_obstacle.translation = navigation.astar.get_point_position(random_point)