extends Node

const TOWER_RADIUS = 0.5

var tower_preload = preload("res://src/gameplay/buildings/tower/tower.tscn")
var tower_modules = { 
		"crossbow": [preload("res://src/gameplay/buildings/tower/modules/crossbow_1.tscn"), preload("res://src/gameplay/buildings/tower/modules/crossbow_2.tscn"),
		preload("res://src/gameplay/buildings/tower/modules/crossbow_3.tscn"), preload("res://src/gameplay/buildings/tower/modules/crossbow_4.tscn")],
		"cannon":
		[preload("res://src/gameplay/buildings/tower/modules/cannon_1.tscn"), preload("res://src/gameplay/buildings/tower/modules/cannon_2.tscn"), 
		preload("res://src/gameplay/buildings/tower/modules/cannon_3.tscn"), preload("res://src/gameplay/buildings/tower/modules/cannon_4.tscn")],
		"spinner": [preload("res://src/gameplay/buildings/tower/modules/spinner_1.tscn"), preload("res://src/gameplay/buildings/tower/modules/spinner_2.tscn"),
		preload("res://src/gameplay/buildings/tower/modules/spinner_3.tscn"), preload("res://src/gameplay/buildings/tower/modules/spinner_4.tscn")],
		"foundation": [preload("res://src/gameplay/buildings/tower/modules/foundation_1.tscn"), preload("res://src/gameplay/buildings/tower/modules/foundation_2.tscn"), 
		preload("res://src/gameplay/buildings/tower/modules/foundation_3.tscn"), preload("res://src/gameplay/buildings/tower/modules/foundation_4.tscn")]
	}
var tower_upgrades = {
		"crossbow": [40, 25, 5, 2], # Скорострельность
		"cannon": [100, 120, 160, 200], # Урон
		"spinner": [1, 2, 5, 10],
		"foundation": [100, 200, 800, 1600],
	}
var traps_preload = {
	"bear_trap": preload("res://src/gameplay/buildings/traps/bear_trap.tscn"),
	"shield_with_spikes": preload("res://src/gameplay/buildings/traps/shield_with_spikes.tscn"),
	"spikes": preload("res://src/gameplay/buildings/traps/spikes.tscn"),
}
var traps_radius = {
	"bear_trap": 0.25,
	"shield_with_spikes": 1,
	"spikes": 1,
}
onready var astar = get_node("/root/Game/Navigation").astar
onready var navigation = get_node("/root/Game/Navigation")
onready var coins_controller = get_node("/root/Game/Controllers/Coin_controller")
var tower_cost = 10

func build_tower(input_position):
	if  coins_controller.money > tower_cost:
		var translation = astar.get_point_position(astar.get_closest_point(input_position))
		var result = navigation.return_points_if_not_collide(translation, Buildings.sizes["tower"])
		if result == []:
			return
		navigation.block_rect_points(result)
		var new_tower = tower_preload.instance()
		get_node("/root/Game/Towers").add_child(new_tower)
		new_tower.translation = translation
		coins_controller.set_money(tower_cost)
	else:
		print("Not enought money")

func buld_trap(trap_name, input_position):
	if Player.spend_item(trap_name) == false:
		return;
	var translation = astar.get_point_position(astar.get_closest_point(input_position))
	var new_trap = traps_preload[trap_name].instance()
	var result = navigation.return_points_if_not_collide(translation, Buildings.sizes[trap_name])
	if result == []:
		return
	new_trap.translation = translation
	get_node("/root/Game/Traps").add_child(new_trap)
	navigation.block_rect_points(result)