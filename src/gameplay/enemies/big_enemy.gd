extends Spatial
#powered by snodack  
enum State {GoingToGate, GoingToTarget, AttackTower, GoAhead}
var state = null
var ZOMBIE_RADIUS = 0.4 # //вынести нахуй это отсюда(почему каждый зомби хранит это)
var TOWER_RADIUS = 0.5 #перенести в  тавер потом
var DETECT_RADIUS = 0.3 # //вынести нахуй это отсюда(почему каждый зомби хранит это)
var DAMAGE = 10
var EPS = 0.125
var DETECT_ANGLE = cos(deg2rad(30)) #//вынести нахуй это отсюда(почему каждый зомби хранит это)
var speed = 0.3
var _hp = 200
#---------------
var target_pos = null # Точка куда идем
var k = 0#Текущая точка
var way = null
const NEARBY_RADIUS = 0.1 #Радиус на сколько надо подойти //вынести нахуй это отсюда(почему каждый зомби хранит это)
#----------------
var angle = Vector2(1, 0)
var target = null
var target_gate = false
var gives_money = 10  #Кол-во денег за убийство в среднем //вынести нахуй это отсюда(почему каждый зомби хранит это)
#----------------

#Плавный поворот
var target_rotation;
var rotation_speed = 5
#----------------

var rng = RandomNumberGenerator.new()

func _ready():
	rng.randomize()
	target_rotation = $Skeleton.rotation_degrees.y
	init()
	#сыграть анимация
	pass
func init():
	DETECT_ANGLE = cos(deg2rad(30))
	ZOMBIE_RADIUS = 0.4
	DETECT_RADIUS = 2
	target = null
	_hp = 500
	state = null
	rotated()

func _process(delta):
	if !state == State.AttackTower:
		translation.x += delta*speed*angle.x
		translation.z += delta*speed*angle.y
	$Skeleton.rotation_degrees.y = lerp($Skeleton.rotation_degrees.y, target_rotation, delta * rotation_speed)
	if state == State.GoingToGate:
		if translation.x > 11:
			target = null
			target_gate = true
			state_to_attack();
		if len(way) == 1 && check_direct():
			state_to_target()
		elif translation.x > 8:
			target_rotation = 90
			angle =Vector2(1,0)
		
		elif Vector2(translation.x, translation.z).distance_to(target_pos) < NEARBY_RADIUS:
			k+=1
			view()
	elif state == State.GoingToTarget:
		if is_instance_valid(target):
			if abs((ZOMBIE_RADIUS + TOWER_RADIUS) - Vector2(translation.x, translation.z).distance_to(Vector2(target.translation.x, target.translation.z))) < EPS:
				state_to_attack()
				pass
		else:
			state_to_gate()
			pass
			
	elif state == State.AttackTower:
		$Skeleton.rotation_degrees.y = target_rotation
		stroke()
	elif state == State.GoAhead:
		state_to_gate()
	elif state == null:
		state_to_gate()

func tower_create():
	state_to_gate()

func state_to_gate():
	get_path()
	if way !=null: 
		k = 0
		view()
		_set_state(State.GoingToGate)
	else:
		if check_direct():
			state_to_target()
		else:
			state_go_ahead()

func view():
	if len(way) > k:
		var pos = Vector2(translation.x, translation.z)
		var pos2 = Vector2(way[k].x, way[k].z)
		angle =(pos2 - pos).normalized()
		target_pos = pos2
		rotated()
		return
	else:
		state_to_gate()

func check_direct():
	var direction_fun
	var pos = Vector2(translation.x, translation.z)
	for node in get_tree().get_nodes_in_group("towers"):
		var pos2 = Vector2(node.translation.x, node.translation.z)
		if pos.distance_to(pos2) < DETECT_RADIUS:
			direction_fun = (pos2 - pos).normalized()
			var dot_product = direction_fun.dot(angle)
			if dot_product > DETECT_ANGLE:
				return true
	return false

func find_target():
	var direction_fun
	var near_target = null
	var pos = Vector2(translation.x, translation.z)
	var tow = get_tree().get_nodes_in_group("towers")
	for node in tow:
		var pos2 = Vector2(node.translation.x, node.translation.z)
		if pos.distance_to(pos2) < DETECT_RADIUS && (near_target == null || pos.distance_to(pos2) < pos.distance_to(Vector2(near_target.translation.x, near_target.translation.z))) :
			direction_fun = (pos2 - pos).normalized()
			var dot_product = direction_fun.dot(angle)
			if dot_product > DETECT_ANGLE:
				near_target = node
				angle = direction_fun
				continue
	return near_target

func rotated():
	target_rotation = 90 - rad2deg(atan2(angle.y, angle.x))

func attack_wall():
	get_parent().is_stage_finish()

func attack():
	if (target == null || target_gate):
		angle = Vector2(1,0)
	else:
		angle = (Vector2(target.translation.x, target.translation.z) - Vector2(translation.x, translation.z)).normalized()
	rotated()

func stroke():
	set_process(false)
	var t = Timer.new()
	t.set_wait_time(1)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	t.queue_free()
	set_process(true)
	if (target == null && target_gate):
		#нанесение урона замку
		get_node("/root/Game/GUI")._damage(DAMAGE, self)
		return;
	if is_instance_valid(target):
		get_node("/root/Game/DamageSystem").deal_damage(target, DAMAGE, self)
		pass
	if !is_instance_valid(target):
		target = null
		state_to_gate()
	set_process(true)

func _damage(amount, _source):
	_hp -= amount
	if _hp <= 0:
		self.die()

func die():
	get_node("/root/Game/Controllers/Coin_controller").get_money(gives_money + rng.randi_range(-2,2))
	get_parent().is_stage_finish()
	self.queue_free()

func get_path():
	k = 0
	way = get_node("/root/Game/Navigation").get_path_for_enemy(translation)

func _set_state(value):
	state = value
	if state == State.AttackTower:
		$Skeleton/AnimationPlayer.play("Attack")
	else:
		$Skeleton/AnimationPlayer.play("Walk")
	

func state_to_target():
	target = find_target()
	attack()
	_set_state(State.GoingToTarget)

func state_to_attack():
	attack()
	_set_state(State.AttackTower)

func state_go_ahead():
	if state != State.GoAhead:
		rotated()
		_set_state(State.GoAhead)

func tower_broked():
	pass