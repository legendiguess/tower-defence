extends Spatial

onready var camera = get_node("Camera")
onready var navigation = $Navigation
onready var building_system = $BuildingSystem
onready var upgrade_menu = $GUI/UpgradeMenu
onready var touch_handler = $TouchHandler

var preload_scenes = {
	"cannonball_and_arrow": preload("res://src/gameplay/ammo/cannonball_and_arrow.tscn")
}

var preload_scripts = {
	# Скрипты для сцены cannonball_and_arrow
	"auto_ammo": preload("res://src/gameplay/ammo/auto_ammo.gd"),
	"hand_control_ammo": preload("res://src/gameplay/ammo/hand_control_ammo.gd"),
	#
}

func _ready():
	get_node("LevelLoader").load_level(0)