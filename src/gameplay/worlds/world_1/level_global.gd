extends Node

onready var navigation = get_node("/root/Game/Navigation")
var current_stage
var stages = ["res://src/gameplay/worlds/world_1/level1.tscn"]
var stages_zombies = [[10, 5], [15, 5], [26, 6]]
var stages_time = [10, 10, 10]
var stage = 0
const FLOAT_EPSILON = 0.00001

func new_stage():
	if current_stage:
		current_stage.free()
		stage+=1
	current_stage = load(stages[stage]).instance()
	get_node("/root/Game/").add_child(current_stage)
	for obstacle in current_stage.get_node("Obstacles").get_children():
		var position = navigation.astar.get_point_position((navigation.astar.get_closest_point(obstacle.translation)))
		var result
		if !compare_floats(obstacle.rotation_degrees.y, 0) and compare_floats(fmod(abs(obstacle.rotation_degrees.y), 90), 0):
			result = navigation.return_points_if_not_collide(position, Vector2(obstacle.obstacle_size_in_points.y, obstacle.obstacle_size_in_points.x))
		else:
			result = navigation.return_points_if_not_collide(position, obstacle.obstacle_size_in_points)
		if result != []:
			navigation.block_rect_points(result)

func _ready():
	pass
	
static func compare_floats(a, b):
    return abs(a - b) <= FLOAT_EPSILON